package cn.seventree.file.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import cn.seventree.file.entry.FileConfig;

import java.net.URI;

/**
 * spring 配置文件
 *
 * @author meikehuan
 * @date 2016-07-12
 * @since 1.0.0
 */
@Configuration
@ComponentScan(basePackages = { "cn.seventree.file.service" })
@PropertySource("classpath:/settings.properties")
public class SpringConfig {
	@Autowired
	private Environment environment;

	@Bean
	public FileConfig fileConfig() {
		FileConfig fileConfig = new FileConfig();
		fileConfig.setOutPath(environment.getProperty("config.outpath"));
		fileConfig.setPath(environment.getProperty("config.rootpath"));
		return fileConfig;
	}
}
