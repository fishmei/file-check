package cn.seventree.file;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import cn.seventree.file.config.SpringConfig;
import cn.seventree.file.service.EntryService;

/**
 * 程序主入口
 *
 * @author meikehuan
 * @date 2016-07-12
 * @since 1.0.0
 */
public class MainEntry {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
		EntryService entryService = applicationContext.getBean(EntryService.class);
		entryService.init();
	}
}
