package cn.seventree.file.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.seventree.file.util.FileHashUtil;
import cn.seventree.file.util.SimpleDirectoryVisitorImpl;

/**
 * 
 * @Description 根据目录下文件生成相应校验文件（用于文件的校验）
 * @author meikehuan
 * @date 2016年7月12日 上午9:14:47
 * @since 1.0.0
 */
public interface EntryService {
	/**
	 * 初始化
	 * 
	 * @param args
	 * @throws IOException
	 */
	public void init() ;
}
