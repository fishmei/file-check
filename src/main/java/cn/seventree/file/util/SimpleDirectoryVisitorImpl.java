package cn.seventree.file.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;


/**
 * 
* @Description 用于遍历对应目录下所有文件
* @author meikehuan 
* @date 2016年7月12日 上午11:38:12 
* @since 1.0.0
 */
public class SimpleDirectoryVisitorImpl extends SimpleFileVisitor<Path> {
    private List<File> list_file =new ArrayList<File>(); 
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exec)
            throws IOException {
        // 访问文件夹之前调用
        //System.out.println("Just visited " + dir);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
            throws IOException {
        // 访问文件夹之后调用
        //System.out.println("About to visit " + dir);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
            throws IOException {
        // 访问文件后调用        
        if (attrs.isRegularFile()){
            list_file.add(file.toFile());    //插入一个List<String>有别的用。 
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc)
            throws IOException {
        // 文件不可访问时调用
        //System.out.println(exc.getMessage());
        return FileVisitResult.CONTINUE;
    }
    
    public List<File> getList() {  
           return list_file;
     }  


}

