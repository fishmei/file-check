package cn.seventree.file.entry;
/**
 * 文件检查配置
 *
 * @author meikehuan
 * @date 2016-07-12
 * @since 1.0.0
 */
public class FileConfig {
	private String path;
	private String outPath;

	public FileConfig() {

	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getOutPath() {
		return outPath;
	}

	public void setOutPath(String outPath) {
		this.outPath = outPath;
	}

}
